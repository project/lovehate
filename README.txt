
Description
===========
Lets users vote 'love' or 'hate' on the terms of a particular taxonomy vocabulary. Each user's loves and hates are displayed on their profile, and the /lovehate page shows the highest and lowest ranked terms. 

Requirements
============
VotingAPI.module, and Taxonomy.module

Credits
=======
Sponsored by the fine folks at Lullabot! http://www.lullabot.com